// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'chat-with-angular-d4adb',
    appId: '1:957297086038:web:9a8fc7631d4bd2192bb5f7',
    storageBucket: 'chat-with-angular-d4adb.appspot.com',
    apiKey: 'AIzaSyAWmemAUjTa1xrQdtEGMXUoeyRz6JwnJR8',
    authDomain: 'chat-with-angular-d4adb.firebaseapp.com',
    messagingSenderId: '957297086038',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
